# Movies Discovery

A wonderful application to discover new movies.

See deployment here: https://nicolas-van.gitlab.io/movies-discovery

## Quickstart guide

```
npm install
npm run start
```
