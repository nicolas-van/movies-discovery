
import './moviesDiscovery';
import './popularMovies';
import './search';
import './movie';
import './movieMiniature';
