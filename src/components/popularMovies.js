
import module from '../module';
import html from './popularMovies.html';
import './popularMovies.scss';
import _ from 'lodash';
import config from '../../config.json';
import * as rx from 'rxjs';
import * as rxop from 'rxjs/operators';

/*
  A page to display the current most popular movies.
*/
module.component('popularMovies', {
  template: html,
  controller: ["moviesDatabase", "$scope", function (moviesDatabase, $scope) {
    this.currentPage = 0;
    this.movies = [];
    
    const queryNext = async () => {
      this.currentPage += 1;
      const movies = await moviesDatabase.getPopular(this.currentPage);
      this.movies = this.movies.concat(movies.map((movie) => {
        return _.assign({}, movie, {
          posterUrl: movie.poster_path ? `${config.tmdbImagesRoot}/original${movie.poster_path}` : null,
        });
      }));
      $scope.$apply();
    };
    queryNext();
    
    const scroll = rx.Observable.create((observer) => {
      this.scroll = observer.next.bind(observer);
    });
    scroll.pipe(rxop.throttleTime(1000)).subscribe(queryNext);
  
  }]
});
