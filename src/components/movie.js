
import module from '../module';
import html from './movie.html';
import './movie.scss';
import _ from 'lodash';
import config from '../../config.json';

/*
 A page to display a movie.
*/
module.component('movie', {
  template: html,
  controller: ["moviesDatabase", '$routeParams', '$scope', function (moviesDatabase, $routeParams, $scope) {
    const query = async () => {
      const movie = await moviesDatabase.getMovie($routeParams.id);
      this.movie = _.assign({}, movie, {
        posterUrl: movie.poster_path ? `${config.tmdbImagesRoot}/original${movie.poster_path}` : null,
        backdropUrl: movie.backdrop_path ? `${config.tmdbImagesRoot}/original${movie.backdrop_path}` : null,
      });
      $scope.$apply();
    };
    query();
  }]
});
