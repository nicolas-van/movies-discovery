
import module from '../module';
import html from './movieMiniature.html';
import './movieMiniature.scss';

/*
 A movie miniature
*/
module.component('movieMiniature', {
  template: html,
  bindings: {
    movie: '<',
  },
  controller: [function () {
    console.log("yop", this.hero);
  }]
});
