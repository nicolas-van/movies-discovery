
import angular from 'angular';
import module from '../module';
import html from './moviesDiscovery.html';
import './moviesDiscovery.scss';
import _ from 'lodash';
import config from '../../config.json';

/*
  The root component of the application.
*/
module.component('moviesDiscovery', {
  template: html,
  controller: [function () {

  }]
});
