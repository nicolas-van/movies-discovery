
import module from '../module';
import html from './search.html';
import './search.scss';
import _ from 'lodash';
import config from '../../config.json';
import * as rx from 'rxjs';
import * as rxop from 'rxjs/operators';

/*
  A page to search for movies.
*/
module.component('search', {
  template: html,
  controller: ["moviesDatabase", "$scope", function (moviesDatabase, $scope) {
    this.search = "";
    this.currentPage = 0;
    this.movies = [];
    
    const searchNext = async (restart = false) => {
      if (restart) {
        this.currentPage = 0;
        this.movies = [];
      }
      if (this.search === "") {
        this.movies = [];
      } else {
        this.currentPage += 1;
        const movies = await moviesDatabase.search(this.search, this.currentPage)
        this.movies = this.movies.concat(movies.map((movie) => {
          return _.assign({}, movie, {
            posterUrl: movie.poster_path ? `${config.tmdbImagesRoot}/original${movie.poster_path}` : null,
          });
        }));
      }
      $scope.$apply();
    };
    
    const search = rx.Observable.create((observer) => {
      this.changeSearch = observer.next.bind(observer);
    });
    search.pipe(rxop.auditTime(1000)).subscribe((s) => {
      searchNext(true);
    });
    
    const scroll = rx.Observable.create((observer) => {
      this.scroll = observer.next.bind(observer);
    });
    scroll.pipe(rxop.throttleTime(1000)).subscribe(() => {
      searchNext();
    });
    
  
  }]
});
