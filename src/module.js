import angular from 'angular';
import 'angular-route';
import 'ng-infinite-scroll';

/*
  The angular module.
*/
var app = angular.module('moviesDiscoveryApp', ['ngRoute', 'infinite-scroll']);

app.config(['$locationProvider', '$routeProvider',
  function config($locationProvider, $routeProvider) {
    $locationProvider.hashPrefix('!');
    $routeProvider.
      when('/search', {
        template: '<search></search>'
      }).
      when('/movie/:id', {
        template: '<movie></movie>'
      }).
      when('/', {
        template: '<popular-movies></popular-movies>'
      });
  }
]);

export default app;
