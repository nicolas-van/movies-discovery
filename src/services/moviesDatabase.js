
import module from "../module";
import config from "../../config.json";

/*
  A service to interrogate the tmdb api.
*/
module.factory('moviesDatabase', ['$http', function($http) {
  return {
    /*
      Gets a list of popular movies.
      
      page: the asked page
    */
    getPopular: async (page = 1) => {
      const res = await $http.get(
        `${config.tmdbRoot}/movie/popular?api_key=${config.tmdbApiKey}&language=en&page=${page}`);
      return res.data.results;
    },
    /*
      Gets a specific movie by id.
      
      id: the id of the movie
    */
    getMovie: async (id) => {
      const res = await $http.get(
        `${config.tmdbRoot}/movie/${id}?api_key=${config.tmdbApiKey}`);
      return res.data;
    },
    
    /*
      Search for movies.
      
      query: the search query
      page: the asked page
    */
    search: async (query, page = 1) => {
      const res = await $http.get(
        `${config.tmdbRoot}/search/movie?language=en&query=${encodeURI(query)}&api_key=${config.tmdbApiKey}&page=${page}`);
      return res.data.results;
    },
  };
}]);
